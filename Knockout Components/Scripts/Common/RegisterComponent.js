﻿;(function () {
    function RegisterComponent(component) {
        var componentRoot = "/components/" + component + "/";

        ko.components.register(component, {
            viewModel: window.Components[component],
            template: { require: "text!" + componentRoot + component + ".html" }
        });
    }

    window.Components = [];
    window.RegisterComponents = RegisterComponents;

    /* All components which need to be available on site load should be registered here */

    var ComponentList = [
        "activity-box",
        "basic-navbar"
    ];

    function RegisterComponents(list)
    {
        if (!list)
            list = ComponentList;

        if (!require.s.contexts._.config.paths.text) {
            require.config({
                paths: {
                    text: "/Scripts/text"
                }
            });
        }

        for (var i = 0 ; i < list.length ; i++) {
            RegisterComponent(list[i]);
        }
    }
})();