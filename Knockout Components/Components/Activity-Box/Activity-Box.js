﻿; (function () {
    function ActivityBoxViewModel(params) {

        if (!params) return;

        if (params.data) {
            params = params.data;
        }

        this.categoryTitle = ko.observable(params.categoryTitle);
        this.activityTitle = ko.observable(params.activityTitle);
        this.type = ko.observable(params.type);
        this.hours = ko.observable(params.hours);
        this.description = ko.observable(params.description);

        this.Bookmark = bookmark;
        this.AddToBasket = addToBasket;
    }

    function bookmark(data, event)
    {
        var target = $(event.currentTarget);
        var bookmark = target.children("i");

        if (bookmark.hasClass("fa-bookmark-o"))
        {
            bookmark.removeClass("fa-bookmark-o");
            bookmark.addClass("fa-bookmark");
        }
        else
        {
            bookmark.removeClass("fa-bookmark");
            bookmark.addClass("fa-bookmark-o");
        }
    }

    function addToBasket(data, event)
    {
        var target = $(event.currentTarget);
        var bookmark = target.children(".fa-shopping-cart");

        if (bookmark.hasClass("fa-shopping-cart")) {
            bookmark.removeClass("fa-shopping-cart");
            bookmark.addClass("fa-check");
            bookmark.siblings(".fa-plus").hide();
        }
    }

    window.Components["activity-box"] = ActivityBoxViewModel;
})();