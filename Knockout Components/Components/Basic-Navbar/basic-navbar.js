﻿; (function () {
    function BasicNavbarViewModel(params) {
        if (!params)
        {
            return;
        };

        if (params.data) {
            params = params.data;
        }

        this.logoText = ko.observable(params.logoText);
        this.logoHref = ko.observable(params.logoHref);
        this.links = ko.observableArray(params.links);
    }

    window.Components["basic-navbar"] = BasicNavbarViewModel;
})();