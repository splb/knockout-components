﻿using System.Web.Mvc;

namespace Knockout_Components.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ArticleBox()
        {
            return View("article-box");
        }
    }
}